
import clickOutside from './clickOutside'
export default {
    install(Vue){
        Vue.directive('unicom-click-outside',{
            ...clickOutside
        })
    }
}
