import Vue from 'vue'
import App from './App.vue'
import UnimedComponents from 'unimed-components'
import Diretivas from './Diretivas'

Vue.use(Diretivas)
Vue.use(UnimedComponents)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
